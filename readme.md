# TERRAGON FERRY Website

HTML project created with bootstrap4, scss and simple html

## How to run?

* download the repo
* `npm install`
* `sh startServer.sh`
* Open a web browser at: http://localhost:8000

Enjoy :)